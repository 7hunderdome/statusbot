# StatusBot

A simple discord bot for the Seedsow/Snowreap servers. 

-Watches for a running instance/window name (thus allowing to watch both servers) and alerts a specific discord channel in the event of downtime. 
-Will alert if the server is able to restart itself or continue to notify of downtime. 
-Built in ability to disable bot spam for each server via Discord command. 

NOTE: This script is not designed for external use but you are free to utilize and modify the code for your own Discord bot. There is limited commentary within the script and I recommend checking out a python discord bot tutorial for any questions on how to use discord.py in your own bot or for your own purposes.

Link for discord.py:
https://pypi.org/project/discord.py/