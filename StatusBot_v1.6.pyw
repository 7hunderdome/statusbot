from discord.ext import tasks
import discord
import win32ui
import pystray
import PIL.Image

from datetime import datetime
import os
import sys
from multiprocessing import Process

# DEVIOUS GLOBAL VARIABLE SECTION
########################################################################################################################
#Seedow discChannel = 807848007877263371
discChannel = 806206407996604467 
# Enable developer tools under Settings->Appearance, right click on the appropriate
# channel, then COPY ID for the appropriate channel to send our alerts/spam to.
########################################################################################################################
Seedsow = 'GDL - Classic Dereth - Seedsow'  # Name of the server window **EXACTLY** as it appears in Windows.
#Seedow ssGeneral = 436870348676268053
ssGeneral = 806220124621111296 #test IDs
SScount = 0 
SSspam = True  # Enables spam by default on startup, do not change.
SSserverStatus = True  # Assume the server is up on bot startup, bot will quickly determine if it is not.
SSinit = True
########################################################################################################################
Snowreap = 'GDL - Classic Dereth - Snowreap'  # Name of the server window **EXACTLY** as it appears in Win.
#Seedsow srGeneral = 436870536883339266
srGeneral = 806220144489136178 #test IDs
SRcount = 0 
SRspam = True  # Enables spam by default on startup, do not change.
SRserverStatus = True  # Assume the server is up on bot startup, bot will quickly determine if it is not.
SRinit = True
image = PIL.Image.open("StatusBot.png")
########################################################################################################################

def seedsowExists():
    try:
        if win32ui.FindWindow(None, Seedsow):
            return "```js\n \"Seedsow status:  **ONLINE**\" ```"  # Send to discord in a lovely green.
    except win32ui.error:
        return '```css\n [Seedsow status: **OFFLINE**\n] ```'  # Send to discord in an alerting red.


def snowreapExists():
    try:
        if win32ui.FindWindow(None, Snowreap):
            return "```js\n \"Snowreap status:  **ONLINE**\" ```"  # Send to discord in a lovely green.
    except win32ui.error:
        return '```css\n [Snowreap status: **OFFLINE**\n] ```'  # Send to discord in an alerting red.


############################ICON##############################
def on_clicked(icon, item):
        if str(item) == "Exit":
            icon.stop()
            os._exit(1)
            #pid = os.getpid()
            #os.kill(pid, 0) 
##############################################################


client = discord.Client()

@client.event
async def on_ready():
    
    icon = pystray.Icon("Status Bot", image, menu=pystray.Menu(
        pystray.MenuItem(f"Logged in: {client.user}", None),
        pystray.MenuItem("Exit", on_clicked)        
        ))
    
    icon.run_detached()

    global discChannel
    await client.change_presence(status=discord.Status.online, activity=discord.Game(name="Seedsow: ONLINE"))
    seedsowrunning.start()
    snowreaprunning.start()
    print('*#* Login to Discord was successful, appearing as user: {0.user} *#*'.format(client))
    print('')
    print('Status Bot initialized. Watch has begun on Seedsow and Snowreap. Logging available in [server]down.txt')
    print('')
    channel = client.get_channel(discChannel)
    await channel.send('```ini\n [BEEP BOOP, bot initialized. Detecting server status...] ```')

@tasks.loop(seconds=2)  # Seedsow
async def seedsowrunning():
    global SScount
    global SSspam
    global discChannel
    global SSserverStatus
    global SSinit

    channel = client.get_channel(discChannel)
    try:
        if win32ui.FindWindow(None, Seedsow):
            if SSinit:
                SSinit = False
                await channel.send('```js\n \"Seedsow seems to be online! Status: **ONLINE**\" ```')
            if SScount >= 1:
                await client.change_presence(status=discord.Status.online,
                                             activity=discord.Game(name="Seedsow: ONLINE"))
                await channel.send('```js\n \"Seedsow seems to be back! Status: **ONLINE**\" ```')
                ss = open("SeedsowDown.txt", "a")  # logging reconnect to [server]down.txt
                ss.write(
                    datetime.now().strftime("**%B %d, %Y %H:%M:%S SUCCESS: Server appears to be ONLINE again ****\n"))
                ss.close()  # always close files to prevent memory leaks and 1000 opened copies of the same file.
                SScount = 0  # or use with open for automatic file closing.
                SSspam = True
                SSserverStatus = True

    except win32ui.error:
        SScount += 1
        SSserverStatus = False
        if SScount % 5 == 0 or SScount == 1:
            ss = open("SeedsowDown.txt", "a")  # logging our disconnects to [server]down.txt
            ss.write(datetime.now().strftime(
                "**%B %d, %Y %H:%M:%S Attempts since last online: {0} : Seedsow appears to be offline ****\n".format(
                    SScount)))
            ss.close()  # always close files to prevent memory leaks and 1000 opened copies of the same file.
        if SScount == 1 and not SSinit:
            await channel.send(
                '```css\n[Seedsow has stopped/crashed. Server may attempt to reboot itself.\n I will update if I '
                're-establish a connection.\n Please use !disable ss if you are working on Seedsow to prevent '
                'this spam.]```')
        if SSinit:
            await channel.send(
                '```css\n[Bot has just initialized but Seedsow shows to be down! Status **OFFLINE** Use !disable ss '
                'to prevent further spam.]\n```')
            SSinit = False
            pass
        if SSspam:
            await client.change_presence(status=discord.Status.do_not_disturb,
                                         activity=discord.Game(name="Seedsow: OFFLINE"))

        if SScount % 31 == 0 and SSspam:
            await channel.send(
                '```css\n [**ALERT!*** - SEEDSOW IS STILL OFFLINE!\n Send !disable to prevent this spam. I will '
                'automatically re-enable spam after successful reconnection.] ```')


@tasks.loop(seconds=2)  # Snowreap
async def snowreaprunning():
    global SRcount
    global SRspam
    global discChannel
    global SRserverStatus
    global SRinit

    channel = client.get_channel(discChannel)
    try:
        if win32ui.FindWindow(None, Snowreap):
            if SRinit:
                SRinit = False
                await channel.send('```js\n \"Snowreap seems to be online! Status: **ONLINE**\" ```')
            if SRcount >= 1:
                await client.change_presence(status=discord.Status.online,
                                             activity=discord.Game(name="Snowreap: ONLINE"))
                await channel.send('```js\n \"Snowreap seems to be back! Status: **ONLINE**\" ```')
                sr = open("SnowreapDown.txt", "a")  # logging reconnect to [server]down.txt
                sr.write(
                    datetime.now().strftime("**%B %d, %Y %H:%M:%S SUCCESS: Snowreap appears to be ONLINE again ****\n"))
                sr.close()  # always close files to prevent memory leaks and 1000 opened copies of the same file.
                SRcount = 0  # or use with open for automatic file closing.
                SRspam = True
                SRserverStatus = True

    except win32ui.error:
        SRcount += 1
        SRserverStatus = False
        if SRcount % 5 == 0 or SRcount == 1:
            sr = open("SnowreapDown.txt", "a")  # logging our disconnects to [server]down.txt
            sr.write(datetime.now().strftime(
                "**%B %d, %Y %H:%M:%S Attempts since last online: {0} : Seedsow appears to be offline ****\n".format(
                    SScount)))
            sr.close()  # always close files to prevent memory leaks and 1000 opened copies of the same file.
        if SRcount == 1 and not SRinit:
            await channel.send(
                '```css\n[Snowreap has stopped/crashed. Snowreap may attempt to reboot itself.\n I will update if I '
                're-establish a connection.\n Please use !disable sr if you are working on Snowreap to prevent this '
                'spam.]```')
        if SRinit:
            await channel.send(
                '```css\n[Bot has just initialized but Snowreap shows to be down! Status **OFFLINE** Use !disable sr '
                'to prevent further spam.]\n```')
            SRinit = False
            pass
        if SRspam:
            await client.change_presence(status=discord.Status.do_not_disturb,
                                         activity=discord.Game(name="Snowreap: OFFLINE"))

        if SRcount % 31 == 0 and SRspam:
            await channel.send(
                '```css\n [**ALERT!*** - SNOWREAP IS STILL OFFLINE!\n Send !disable to prevent this spam. I will '
                'automatically re-enable spam after successful reconnection.] ```')


@client.event
async def on_message(message):
    global SSspam
    global SSserverStatus
    global SRspam
    global SRserverStatus
    global discChannel
    global srGeneral
    global ssGeneral

    if message.author == client.user:
        return

    if message.content.lower() == '!help':
        await message.channel.send(
            '`Commands I can respond to:\n !about - Info about this bot.\n !disable - Disable spam until the next '
            'reset.\n !help - This menu.\n !log -  Shows the last line of the log file.\n !seedsow - links to '
            'Serafino.ddns.net for quick access. \n !status [seedsow/ss, snowreap/sr] - Shows current server\n  status. '
            '(Example: !status seedsow / !status sr)\n\n'
            'Please request more features from my developer (see !about).\n`')

    if message.content.lower() == '!about':
        await message.channel.send(
            '`This bot tracks uptime for the Seedsow/Snowreap servers and reports that status when appropriate. ('
            'Developed by Thunderdome)`')

    if message.content.lower() in ['!log Seedsow', '!log ss']:  # Seedsow
        if message.channel.id == discChannel:
            with open('SeedsowDown.txt', 'r') as ss:
                last_line = ss.readlines()[-1]
            await message.channel.send('`*Last recorded Seedsow log entry:* \n {0}`'.format(last_line))

        else:
            await message.channel.send(
                '`Forbidden request: This command must be used in the proper channel (Advocates only)!`')
            return
    if message.content.lower() in ['!log Snowreap', '!log sr']:  # Snowreap
        if message.channel.id == discChannel:
            with open('SnowreapDown.txt', 'r') as sr:
                last_line = sr.readlines()[-1]
            await message.channel.send('`*Last recorded Snowreap log entry:* \n {0}`'.format(last_line))

        else:
            await message.channel.send(
                '`Forbidden request: This command must be used in the proper channel (Advocates only)!`')
            return
    if message.content.lower() in ['!status seedsow', '!status ss']:  # seedsow
        await message.channel.send(seedsowExists())

    if message.content.lower() in ['!status snowreap', '!status sr']:  # Snowreap
        await message.channel.send(snowreapExists())

    if message.content.lower() == '!seedsow':  # Only need 1 for these purposes
        await message.channel.send('https://Serafino.ddns.net/')

    if message.content.lower() in ['!disable seedsow', '!disable ss']:  # Seedsow
        if message.channel.id != discChannel:
            await message.channel.send(
                '`Forbidden request: This command must be used in the proper channel (Advocates only)`')
            return
        if not SSspam:
            await message.channel.send('```fix\nSpam has already been disabled for Seedsow.\n```')
            return
        if SSserverStatus:
            await message.channel.send('`Forbidden request: I cannot disable spam while the server is active!`')
            return
        else:
            SSspam = False
            await message.channel.send(
                '```fix\nSpam disabled for Seedsow! I hope you know what you\'re doing. No matter, I will re-enable '
                'spam automagically on next successful connection.\n```')  # Send to discord in a neutral yellow.
            await client.change_presence(status=discord.Status.idle, activity=discord.Game(name="ADVOCATE DISABLED"))

    if message.content.lower() in ['!disable snowreap', '!disable sr']:  # Snowreap
        if message.channel.id != discChannel:
            await message.channel.send(
                '`Forbidden request: This command must be used in the proper channel (Advocates only)`')
            return
        if not SRspam:
            await message.channel.send('```fix\nSpam has already been disabled for Snowreap.\n```')
            return
        if SRserverStatus:
            await message.channel.send('`Forbidden request: I cannot disable spam while the server is active!`')
            return
        else:
            SRspam = False
            await message.channel.send(
                '```fix\nSpam disabled for Snowreap! I hope you know what you\'re doing. No matter, I will re-enable '
                'spam automagically on next successful connection.\n```')  # Send to discord in a neutral yellow.
            await client.change_presence(status=discord.Status.idle, activity=discord.Game(name="ADVOCATE DISABLED"))

    if message.channel.id == ssGeneral:  # Seedsow
        if 'boom' == message.content.lower():
            await message.channel.send(seedsowExists())

        a = ['server down', 'server dead', 'server boom', 'server crash', 'server crash?']
        if message.content.lower() in a or message.content.lower() == 'server status' or message.content.startswith(
                'crash?'):
            await message.channel.send(seedsowExists())

    if message.channel.id == srGeneral:  # Snowreap
        if 'boom' == message.content.lower():
            await message.channel.send(snowreapExists())

        a = ['server down', 'server dead', 'server boom', 'server crash', 'server crash?']
        if message.content.lower() in a or message.content.lower() == 'server status' or message.content.startswith(
                'crash?'):
            await message.channel.send(snowreapExists())


def main():
    return client.run(os.getenv('TOKEN'))
    # Store your bot's special Discord token in Windows Environment variables under the name TOKEN


try:
    main()  # Execute main, pray to Pesci.
except Exception as e:
    f = open("botdown.txt", "a")  # Log bot failures to a file so that we can see why/when the bot wasn't running.
    f.write(datetime.now().strftime(
        "\n**%B %d, %Y %H:%M:%S Internet connection lost? I cannot connect to Discord at this time. Error supplied: "
        "{0} ****\n\n".format(e)))
    f.close()
    sys.exit(0)
    # exit, and use our RestartOnCrash program to reboot the script. This will suffice for network downtime for now.
